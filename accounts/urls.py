from django.urls import path
from accounts.views import signup_function, login_function, logout_function

urlpatterns = [
    path("login/", login_function, name="login"),
    path("logout/", logout_function, name="logout"),
    path("signup/", signup_function, name="signup"),
]
