from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            newTask = form.save(False)
            newTask.assignee = request.user
            newTask.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    myTasks = Task.objects.filter(assignee=request.user)
    context = {"myTasks": myTasks}
    return render(request, "tasks/myTasks.html", context)
